```sass
@media screen and (prefers-reduced-motion: reduce) { // or @media screen and (prefers-reduced-motion)

  // .. code
}

@media screen and (prefers-reduced-motion: no-preference) {
  // ... code
}
```

The previous snippet can be used to remove animations in the UI. This can be beneficial for some users that animations can create distraction (accessibility).

There's 2 different approaches:
- Turn all animations off
- Make all animations really fast (impossible to percieve by human eye)

Each approach depends on the projects UX and user testing.
_Note: provide the ability to toggle them through as an user input is a good practice as well_