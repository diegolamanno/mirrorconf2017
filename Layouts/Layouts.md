# Old school approach
Containers box layout only takes care of floating elements if: 
- `overflow: hidden` but this could generated some issues. It is a better approach with... 
- `display: float-root` created to fix issues with overflow hidden. _check examples on slides_
- Worth to read _Webfont Handbook_ by Bram Stein from Book Apart