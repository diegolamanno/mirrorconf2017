# Main concepts to design for VR
-** Presence:** Make users feel they are in there and part of the imaginary world (avatars)
- **Agency:** _Input is king_. Combining the real world and VR helps to fill the gap for the lack of adoption. Example: things created on VR can be posted on social media and someone can call the user from a phone inside a VR experience. There is 3 major types of inputs for VR:
  -  gaze
  -  3dof
  -  6dof
- **Confort:** Techniques to make sure the user doesn't experience sickness or tireness while inside the VR headset. Examples are:
  - phi phenomenom
  - optical flow

More information can be found at https://www.facebook.design/vr

![gabriel-valdivia-notepad.jpg](Design Beyond the screen/resources/17C2AC813B43D01A72E78E77FC88D8F8.jpg)