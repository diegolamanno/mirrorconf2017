# 3 key points
- **Find a problem and the questions.** You could derail entirely an exploration by picking the wrong (or not real) problems or questions. 
- **Work with users to discover answers** Make sure you are targeting your right users and not getting answers from people that are not relevant to your project at all
- **Analyze & discuss with the team** Most important phase of this process. 

** ITERATE **

![aros-bilgen-notepad.jpg](User researching for everyone/resources/722CB226077BD93EF38F358CA000667A.jpg)