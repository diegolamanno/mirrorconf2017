# MirrorConf 2017

## Workshops
- [The Front-End Espresso Shot](The front-end Espresso Shot/The front-end Espresso Shot.md) - _Sara Soueidan_
    - [Icon fonts (SVG)](Icon fonts (SVG)/Icon fonts (SVG).md)
    - [Support Query](Support Query/Support Query.md)
    - [Remove animations for accessibility](Remove animations for accessibility/Remove animations for accessibility.md)
    - [Scaling Images](Scaling Images /Scaling Images.md)
    - [Responsive fonts](Responsive fonts/Responsive fonts.md)
    - [Layouts](Layouts/Layouts.md)
    - [Questions Sara](Questions Sara/Questions Sara.md)
- [Responsive Interface Design Bootcamp ](Responsive Interface Design Bootcamp/Responsive Interface Design Bootcamp.md) - _Vitaly Friedman_

## Conference Day 1
- [How to Tell When You're Tired](Evaluating technology : Refractions/Evaluating technology   Refractions.md) - _Frank Chimero_
- [The SVG of .svg](The <svg> of .svg/The  svg  of .svg.md) - _Sara Soueidan_
- [User Research for Everyone](User researching for everyone/User researching for everyone.md) - _Aras Bilgen_
- [Design for Emotion](Design for Emotion/Design for Emotion.md) - _Henry Daubrez_
- [Solving Layout Problems with CSS Grid and Friends](Solving layout problems with CSS grid and friends/Solving layout problems with CSS grid and friends.md) - _Rachel Andrew_
- A Little Story About A Big Band Redesign - _Vitaly Friedman_

## Conference Day 2
- [Design Beyond the Screen](Design Beyond the screen/Design Beyond the screen.md) - _Gabriel Valdivia_
- [Evaluating Technology](Evaluating technology : Refractions/Evaluating technology   Refractions.md) - _Jeremy Keith_
- [What's Your Roadmap](What’s your roadmap?/What’s your roadmap.md) - _C. Todd Lombardo_
- [Web Performance Kinetosis](Web performance kinotis/Web performance kinotis.md) - _Eduardo Boucas_
- Home - _Claudio Guglieri_
- [Let's Work Together](Let’s work together/Let’s work together.md) - _Brad Frost_

## Photo Album
https://photos.app.goo.gl/jKQT8CtS6N7g08dg1