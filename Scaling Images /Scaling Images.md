# Object-fit
Use this property on an image and it will create a container-ish that can be styled to postion the image in different ways. _Note: use it as enhancement because it is currently not fully supported_

# Object-postion
Use it side by side with `object-fit`. Works very similar to `background-position`

# Tips
- Note: Check Sara's slides for svg snippet to inject images inside an svg container with full browser support
- The best way to inject `svg` using the object tag because allows you to use a fallback. Example: for an svg infografic, the fallback would be the data