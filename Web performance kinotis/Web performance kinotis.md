#Speaker's deck
https://speakerdeck.com/eduardoboucas/web-performance-kinetosis

# Diagnostic (find the problem)
- How fast is your website (and your competitors)
- What fast means ? understand key metrics
  - first paint, visually complete, DOM interactive, etc
- Find bottlenecks

## Tools to diagnostic
- Browser devs tools
-  Waterfall view and network throttling
- **Test in real devices:** WebpageTest --> network of devices with a variety of conditions around the globe
  - Multiple locations around the world.
  - Difference network conditions
  
_If you product is only used in certain countries. Those are the test you should consider_

# Eyes on the road (fix the problem)
Create a plan, it is really easy to overlook performance. (How long is it going to take?)
- Revisit render-blocking resouces (files at top of the page)
- Remove unnecesary redirects
- Implement optimised/responsive image
- Lazy load content
- Make good use of caching

_frame arguments less of a tecnichal problem but a business one_
_wpostats.com_

## New mindset
- Performance as a feature, not a nice to have
- **Implications to do it as afertthought:** Good way to sell it to stakeholders
- Performance budget --> Can we afford this feature? Everyone that has some kind of participation in the web app/website knows and understand this concept.

# Larger Windows (don't repeat the problem)
- **Increase visibility of performance results:** show it as key results and release features or failures.
  - This is not just developers who should care about it
- Make it a problem for everyone. So they can feel why it is important

## Continous speed test
To allow the team to keep an eye of performance without becoming a chore
**Paid tools**
- Speedcurve
- Calibre
**Open Soruce**
- Speedtracker

### Speedtracker
- Charts show performance metrics over time
- Data comes from webpagetest, pagespeed and lighthouse
- Configure performance budgets
  - slack notifications
- No infrastructure (runs from a git repo)
- hackable and customizable

![eduardo-boucas-notepad.jpg](Web performance kinotis/resources/B1B04F0DA21814C92ABE432FC39707D0.jpg)