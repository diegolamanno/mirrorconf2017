# Biggest take away
This are really rich designs that bring a ton of personality and truly follows the art direction of the project. However, it is still unclear how do make these designs accessible while keeping a balance and not loosing the feeling and tone of the brand.

Seems that some of those considerations are just not in place because they are disposable websites. For a web app or an ongoing marketing site have a to be a different story. 

_Take a look at kikk festival website for visual example_

![henry-daubrez-notepad.jpg](Design for Emotion/resources/886F3710C69E933FC9542AD6B85B2792.jpg)