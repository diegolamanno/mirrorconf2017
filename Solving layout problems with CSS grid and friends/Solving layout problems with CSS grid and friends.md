# Key concept
CSS grid works from the container in ... _meaning:_ the container given `display: grid` has in consideration all the internal elements that could contain, knowing where to place them. All previous methods we had in the past, were designed in relation of an element and its adjacents on a single line.

![Slice.jpg](Solving layout problems with CSS grid and friends/resources/34CF1D08F4A2D5DE81B4A12B716D4B7E.jpg)

The key difference between the other methods and css grid is that we put the burden of calculating the sizes of each container on the browser rather than on us by usign math in our css. Example: `width: 50%; float: left`

_CSS grid: native grid framework right into the broser_

# Boxes inside CSS grid
Giving the browser the ability to calculate the size of the boxes inside a **css grid container** means you need to declare how those boxes will handle content inside them. This can be done any of these properties depending the case:
- `fit-content`
- `min-content`
- `max-content`

_Check the specification or slides for more information about them_

# Support for old browsers
If you need to support old browsers the upcoming properties can be use to create your layout without affecting **css-grid** because they get bypass when browser detects `display: grid`
- `float` 
- `inline-content`
- `table-cell`: **be careful.** This one adds template for tables around it. Making css-grid go roge and producing white spaces around the element that has this display model (display: table-cell). This white spaces is the _table stuff_ wrapped around and they grow thanks to css-grid.
- `vertical-align`
- `multiple-column layout`

_If need any other property that doesn't get omitted by css-grid, use **feature queries** already explained in Sara's presentation_

# Misc

- _Check out subgrid_ (not in the emplementation yet but should solve some of the challenges of nested grids with css-grid)
- The Mansory layout
- _Check out_  Rachel's new book from _A Book Apart_