# Principles and guidelines

Principles and guidelines help a company to walk together on the same line without each department loosing its soul and do things the way they want. Developers coding, designers desgining, etc. If this principles and guidelines don't exist, each team will take complete different directions.

## Things to look for 
- the culture engine book
- Ubuntu brand guidelines
- Voice and tone guide from mailchimp

# Design systems
- Start with user research to create your design system. This means not only your final users but also the people who will maintain it because those are other customers of this project
- Run a kick-off meeting with all the insights you got from user research. Then prioritize using the teams votes.
- Run drills where developers or designers will build the same piece of UI or business logic to see how different people will solve the same problem (provides good insights). Pair programming is perfect for this.

## Things to look for
- _Sketch libraries_ (new/coming) to keep components in sync 
- Photoshop etiquete
- Frontend guidelines dictionary
- styleguideguide


# Design tokens 
Elements share across multiple properties and can be reuse between the design system (olors, font, borders, etc).
We are talking about:
- Communications
- Native apps
- Marketing site
- Intranet
- Product
All this properties could get benefit from the same design system. Allowing the company to deploy UIs faster across all mediums.

## Things to look for
- Lighting principles from salesforce styleguide
- **theo** from salesforce git CLI 
- brandai

![brad-frost-notepad.jpg](Let’s work together/resources/F35EDA889D39A392DB3A33065D81958D.jpg)