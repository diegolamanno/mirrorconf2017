Support queries are basically _if / else blocks_ inside a CSS stylesheet. If browser supports the tested property, the support block will kick in. If not, this entiry block will be ommited.

```sass
// Support for CSS grid
@supports(display: grid) {
  // ... rules
}

// Support for css variables
@supports(--variable: value) {
  // ... rules
}
```

This is perfect to progressive enhance web applications with _CSS-grid_ or _custom-variables_
_Note: Be careful because it is not supported on IE_.