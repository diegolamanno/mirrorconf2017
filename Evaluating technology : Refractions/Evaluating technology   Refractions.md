# Evaluating technology
[btconf2017 Video (same talk)](https://www.youtube.com/watch?v=sMhzgKUKdBU)
- Normally we tend to ask how well supported is some kind of framework or library. However, this is not the right question to ask. _How does it fail?_ actually is more bulletproof to make sure your application is robust even if the external code fails.
- _Who gets benefit out of it?_ By choosing a specific path while designing an application. If the implementation will have some cost in any way (mb to download, building time, framework change, etc) In case of conflict, users always go first in priority. Think what's the experience of the users first and then any other group (devs, stakeholders, etc)
- What are the assumptions? specially behind one technology

## Quotes
_We shape our tools and therefore tools shape us_

_Humans are allergic to change_

# Refractions
![frank-chimero-notepad.jpg](Evaluating technology : Refractions/resources/C3CC477F879DAC2ED6139A78F553CC19.jpg)