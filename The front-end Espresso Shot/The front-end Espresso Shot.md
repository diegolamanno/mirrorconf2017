# Considerations between pre-processor variables and custom-variables
## Pre-processors
- **Static** get compiled and do not at run time. It doesn't exist in the browser
- **Global** always available everywhere

## Custom properties
- _both *name* and *values* are author-defined_
- They are always declare prefixed.

```sass
{
  --variableName: variableValue;
  --primaryColor: deepPink;
  --secondaryColor: grey;
}
```

Custom variables need to be defined somewhere in the scope. Either **local** or **global**. Using the following definition:

```sass
// Global
:root {
  --primaryCOlor: #ccc
}

// Scoped
nav.navigation {
  --someConfigVar: 2;
}
```

_Note: custom variables are case sensitive_
### Usage
Function `var()`:
- Accepts multiple values separated by comma
- First value is the current value
- The following ones are fallback of a fallback

```sass
// Example 2
.button {
  background-color: var(--color, black);
}
// Example 2
.button {
  background-color: black;
  background-color: var(--color, black);
}
```

### Cancelling value of a custom property
_Not sure why you will need to that but it is another approach to change the default instead of overwriting it_

```sass
// Canceling the value (not sure the use yet)
* {
  --myVar: initital;
}
```

_A big difference between SASS variables and custom variables is. Custom ones can be overwritten across the codebase. Meanwhile SASS are defined in a single entry point and reuse everywhere. You need to create multiple variables to get similar result_

```sass
// Change of custom variable value across multiple scopes
button {
  --baseColor: #ccc;
  background-color : var(--baseColor);
}

footer button {
  --baseColor: #fff;
}

button.red {
  --baseColor: #444;
}
```

### Tips

Normally custom-variables will be difined inside a component class scope. However, if there's something shared across multiple components, it is a good excuse to use a `:root` element to use custom variables across all of them.

By using `calc` to generate values based on **%** allows you to modified custom variables even more. _Note: try to define variables for calc using no units. Then use calc again to inject the unit, see example:_

```sass
 `--myVar: .2 .class { calc( var(--myVar) * 2em )`
```

**Video about CSS-grid**
https://www.youtube.com/watch?v=bpo1WwKa9bg

**Sara's Google doc with workshop references.** https://goo.gl/DkecZn

**Logo SVG example** https://codepen.io/SaraSoueidan/pen/XeaRej

**Custom Variables Buttons example** https://codepen.io/mattslack/pen/qPYpYP

**Image gallery example** https://codepen.io/SaraSoueidan/pen/mBLGOx/

**Speaker deck** https://www.dropbox.com/s/habd5m3bua439ch/Front-End-Espress-Shot.pdf?dl=0

