Mainly based on Michael Riethmuller technique for responsive fonts (article can be found on smashing)

# Considerations
- One of the major problems with this approach is having infinite re-scaling on both ends (small / big). It is important to define those breakpoints where the rule doesn't apply anymore.

- The ratio use to re-scale things is really important as well because this can lead to large line-height or headings. Use modularscale.com as a playground to create the correct ratio.

- Useful to add a comment at the top of projects codebase to tell the modular scale use in the codebase for future updates.

- This approach can be very unstable if it is use on a project without a layer like _normalize.css_ underneath