**Did smashing magazine create a single grid (like bootstrap) or it is declared on a template base (css-grid) ?**
Smashing Magazine doesn't use css-grid at the moment. When the layouts were being created css-grid was not stable to use it. If Sara would've done it today. She would've used css grid on some kind of framework-like style.

**Did you use a framework or tool to create the grid or was done from scratch?**
Done from scratch.

**How is HUGO behaving and how is she using it for her personal site ?**
Sara hasn't seen any performance issue at smashing magazine or her site building on hugo. Smashing uses a very customized webpack to create the different pipelines for builds.

Her personal site uses a more simple approach. Writting blog posts on markdown because she is the end user of that platform. With the right implementation, static sites generators shouldn't be a bottle neck in the process.