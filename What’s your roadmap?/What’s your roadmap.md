# Speker's deck
https://speakerdeck.com/iamctodd/whats-your-roadmap

# Take aways
- Challenge our managers if we are following the principles from agile manifesto. This will help a lot specificaly to organizations that are transitioning into agile
- If you are working on a product. Consider to have the roadmap publicaly available so all your users have an idea what features might be coming to the product in the near future
- _Outcomes are not the same as outputs_
- Interesting book to look for: [Never split the difference](https://www.amazon.ca/Never-Split-Difference-Negotiating-Depended/dp/0062407805) (FBI negotiation book)