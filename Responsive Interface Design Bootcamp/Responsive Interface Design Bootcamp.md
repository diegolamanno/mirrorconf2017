# Link to Vitaly’s resources
www.smashed.by/br17

# Productivity tips
- Teams can write standup tasks. In a way to have accountability of every day beyond the product backlog.
- Replying emails only certains days of the week and try to elimintate them to the minimum

# The balancing act
Things that should be showing on an interface.
- **Obvious.** Always have to be there _CTA_
- **Easy.** Frequent but not always
- **Possible.** Sometimes or rarely (often hidden but no frontal)

_UI tip: The hamburguer icon is a well known but very easy to break pattern. Keep the opening button and close button on the same position. Users tend to click on the same spot to manipulate the visibility of the navigation_

_UI tip 2: isn't crazy to consider vertical media queries to optimize the UX on tablets_

# Patterns to maybe use while reordering information
- Accordion (find the **obvious** and the **easy** underneath)
- Tabs (vertically or horizontal)
- Priority pattern. Show as much content as you can in the navigation (whatever it fits), the rest inside burger/all icon. _(the guardian mobile example)_
- Expose the time sensitive content (**obvious**) on a specific place of the UI. The rest of the information (**possible**) can be hidden behind a search functionality.

# Carousel pattern
User research proved users will get engage with the first slide by a big amount. However, all the rest of the slides get the same engaging values.
This means, the carousel pattern isn't working because hidden content (probably *easy*).

At the same time user research proved that if users get engage into move through the slides they tend to always go through **all** of them. Basically, if you provide good reasons to why users should keep scrolling (supported with correct UX/UI), the carousel pattern can be useful.

## Examples:
- kvellhome, 7h34
- belvedere.at _accordion examples all over_

# eCommerce
1. Provide users with all costs at front in the shopping card. 
2. Create an account. This flow tend to fail most of the time. By just changing the wording and in what moment the user is presented to create the account, could increase convertions.
3. Increasing prices and avoid shipping costs. This eliminates costs _(from point 1)_ issues and provides some marketing strategy against your competitors.
4. If cannot provide free shipping. Estimate shipping costs right away. You can use some APIs to guess the closes rate and then recheck while user is purchasing.
- **The copy/pasting product** Very common user interaction on a shopping site. You could create an automatic pop-up to search for that product while copying to search automatically
- Long pages of products. Combining load more product button (on a first interaction) with infinite scrolling (for upcoming ones) seems to be the best conversation way.
- Write a good reason why you need some of the fields. Specially if they are sensitive like phone, country, gender. This goes to all your forms. More information ask, more friction with possible costumers

## Things to look for
- Austin beerworks (more than 21 example)
- Smashing magazine series of articles: how to make perfect slider, accordion, etc_
- smashing magazine kremlin (governement of russia user case) article

# Maps visualitation
- Maps not always have to be geographicaly accurate. Use other figures to represent the map. This can allow a better responsive UI and more interesting approach.
- Load the map conditionaly (static vs interactive for mobile vs desktop)
- Use `eval()` gmail injection technique, _aka Latency injection_.

# Tables
- Navigation buttons on top of the table.
  - A button to show all columns
  - A dropdown with a list of toggle for every single one column
  - Add buttons on top to move horizontal scroll + horizontal scrolling as always
  - Focus mode button to highlight a specific column. If users are checking data against on column this helps to not lose the important data.
- Tilting headers on tables that might not have lengthy data inside. Allowing the UI to get smaller horizontally.
- Turn rows into accordion cards stack on mobile. This patterns destructs the tables into single blocks for each dataset on mobile
- Fixed headers. One of the most important features to add for flexible tables. Making sure user always knows the context of that column
- Ability to move/drag columns to compare columns that might have to far away between each other and cannot fit on the viewport

# Time selectors
- Mini steppers or mini jumpers for time or dates (similar to buttons to move horizontally on tables)
- When dealing with reservations that are based on time and date it is better to ask time first then the date. This creates a smaller universe of options and better user flow. Provides only the dates that those time slots are available, rather than pick the date and then findout the time isn't available.

# Smashing workflow 
- Design error messages first. Tone and voice
- Use your brand all over the UI even for things like error messages. Find your personality

## The UI stack
- Ideal state (the sketch one)
- Error State
- Partial state (could be wrong or fully functional)
- Loading state (markup loading for impression like facebook)
- Blank state (service workers, copy of the previous session)

# Tips
- Responsive layout behavior. Have a conversation between designers and developers showing how the UI will respond to multiple breakpoints before handoff
- GIVE COMPOENETS NAMES THE ENTIRE TEAM UNDERSTANDS AND KNOWS. _Classic, dev calls it main-nav, designers top-nav, stakeholders top-bar_
- You can use **content editable true** in the `<HTML>` tag to allow marketing people to play with the prototype directly in the browser

# Look for it
- Styleguide and boilerplate pattern for prototyping
- seatgeeks
- seat 3d viewer