# SVGs core concepts
- `viewport` ≈ **border** (size of the svg)
- `canvas` is inside the viewport. It is where the svg renders. Technically infinite but constrained by the `viewbox`.
- `viewbox` = **context** of the `viewport` | `viewport` **context** of page

# Viewbox values

```sass
viewBox= <mix-x> <mix-y> <width> <height>
```

_Find in depth explanation on slides_

Initially when SVGs are exported have same size for viewport and viewbox.

# False transparent images
Serving `jpgs` with the `svg` tag allows you to use a masking technique to serve them with transparency without actually having that in the file. It is pretty much the same as serving a `png` but with lower kb ratio. If you site require's heavy optimization this is a possible technique to save some bandwidth on image requests _Look at the slides for code example_

# SVG inline styling tip
Inline SVGs, on the same page and with the same ID will renderthe same inline styles even if you only apply it to the first one. Look at example:

```sass
// Top of the page. Inline svg with some inline styles applied
<svg id =test style=" ... some css">
.
.
.
.
.
// Latter on the page. Try to render the same svg as before (perfect example could be an icon)
<svg id=test>
// Second instance of the svg test gets the same "...some css" as the first one
```

# Libraries to manipulate SVGs
- snap.svg
- greensock

# Random accessiblity tip
_Don't ever write all caps in your HTML_. Some screen readers will read this letter by letter rather than the entire word.

![sara-soudein-notepad.jpg](The <svg> of .svg/resources/F14A289C468104B25875724CEFB76567.jpg)