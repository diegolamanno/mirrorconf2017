**Good tutorial step by step on Sara's blog post**
_https://www.sarasoueidan.com/blog/icon-fonts-to-svg/_

# Exporting icons
- If you are exporting svgs from a editor. Make sure to leave 1 or 2px around it because **firefox** has a bug that can cut the borders of the svgs
- Save save fitting image to the artboard because all that extra space adds weight to the export
- Default size 300 x 150 svg renders on browsers. _Look at slides to check the code of how improve visibility and accessibility for svg display._

# Automation tools
- grunt-svg-store
- sprite.sh
- icomoon (sara's choice)

# Usage
Normally sprite.svg will be _inline_ on the page to add icons. However, this can be load from an external source but it needs to come from the same domain (if not, it will create cross-domain issues), so CDN can create tons of problems.

Tools like iconmoon provide `js` file to polyfill and able to use external sprites everywhere but it has limitations in terms of styling.

# Tips

- You can use `currentColor` value on css against SVG fills to match the text
- Presentation attributes (like fill) overwrite inherit attributes. Use it wisely to reduce the amount of classes required to style svgs
- Brackets have a plugin to visualize svgs and code side by side (really usefull to edit on the SVGs on the fly _